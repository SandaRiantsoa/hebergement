package com.apk.hebergement.data.model;

public class UserRegister extends User{
    protected boolean isAdmin;

    public UserRegister(String username, String password) {
        super(username,password);
    }

    @Override
    public String toString() {
        return "{" +
                "username:'" + super.username + '\'' +
                ", password:'" + super.password + '\'' +
                ", isAdmin:'" + this.isAdmin + '\'' +
                '}';
    }
}
