package com.apk.hebergement.data.model;

public class Room {
    private String name;
    private String description;
    private String image;
    private boolean availability;
    private int price;
    private String roomNumber;
    private int capacity;

    public String getName() {
        return name;
    }

    public String getDescription() {

        return description;
    }
    public String getDescriptionValue() {

        return "Capacité: "+this.capacity+" personnes. " + description;
    }

    public String getImage() {
        return image;
    }

    public boolean isAvailability() {
        return availability;
    }
    public String availability() {
        if (availability) return "Statut : Disponible actuellement";
        else return "Statut : Indisponible";
    }

    public int getPrice() {
        return price;
    }

    public String priceValue() {
        return "Prix : "+this.price+" Ariary";
    }
    public String room_number() {
        return "Chambre numéro : "+this.roomNumber;
    }
    public String getRoomNumber() {
        return roomNumber;
    }

    public int getCapacity() {
        return capacity;
    }
}
