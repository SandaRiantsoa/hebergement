package com.apk.hebergement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apk.hebergement.data.model.Room;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailFagment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailFagment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private Room room;

    public DetailFagment(Room room) {
        this.room = room;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DetailFagment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailFagment newInstance(Room room) {
        DetailFagment fragment = new DetailFagment(room);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_fagment, container, false);
        TextView title = view.findViewById(R.id.title);
        title.setText(room.getName());
        TextView room_number = view.findViewById(R.id.room_number);
        room_number.setText(room.room_number());
        TextView description = view.findViewById(R.id.description);
        description.setText(room.getDescriptionValue());
        TextView availability = view.findViewById(R.id.availability);
        availability.setText(room.availability());
        TextView price = view.findViewById(R.id.price);
        price.setText(room.priceValue());

        ImageView imageView = view.findViewById(R.id.imageView2);
        Picasso.get().load(room.getImage()).into(imageView);
        return view;
    }
}