package com.apk.hebergement;

import com.apk.hebergement.data.model.User;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Service {
    private static final String API = "https://metal-dog-manuscript.glitch.me/api/";

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static final ApiService API_SERVICE = retrofit.create(ApiService.class);
    public static String sharedPreferences = "SaveToken";


}
