package com.apk.hebergement.data.model;

public class User {
    protected String username;
    protected String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "{" +
                "username:'" + username + '\'' +
                ", password:'" + password + '\'' +
                '}';
    }
}
