package com.apk.hebergement;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Icon;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apk.hebergement.data.model.Room;
import com.apk.hebergement.databinding.FragmentItemBinding;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.function.Function;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Room}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyHomeRecyclerViewAdapter extends RecyclerView.Adapter<MyHomeRecyclerViewAdapter.ViewHolder> {

    private final List<Room> mValues;
    private final FragmentTransaction transaction;

    public MyHomeRecyclerViewAdapter(List<Room> items, FragmentTransaction transaction) {
        this.mValues = items;
        this.transaction = transaction;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(FragmentItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Room room = mValues.get(position);
        holder.mItem = mValues.get(position);
        //holder.mIdView.setImageResource(mValues.get(position).id);

        holder.title.setText(room.getName());
        holder.roomnumber.setText(room.room_number());
        holder.capacity.setText("Capacité : "+room.getCapacity());

        Picasso.get().load(room.getImage()).into(holder.imageview);

        holder.itemToCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new DetailFagment(room);
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView title;
        public final TextView roomnumber;
        public final TextView capacity;
        public final ImageButton itemToCard;
        public final ImageView imageview;
        public Room mItem;

        public ViewHolder(FragmentItemBinding binding) {
            super(binding.getRoot());
            title = binding.title;
            roomnumber = binding.roomnumber;
            imageview = binding.imageView;
            capacity = binding.capacity;
            itemToCard = binding.itemToCard;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + title.getText() + "'";
        }
    }

}