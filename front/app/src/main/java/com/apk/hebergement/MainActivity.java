package com.apk.hebergement;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.view.View;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {
    private static final long SPLASH_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new Handler().postDelayed(() -> {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            SharedPreferences sharedPreferences = getSharedPreferences(Service.sharedPreferences, Context.MODE_PRIVATE);
            String token = sharedPreferences.getString("token", "");

            if (token != "") transaction.replace(R.id.fragment_container, new HomeFragment());
            else transaction.replace(R.id.fragment_container, new Login());
            transaction.replace( R.id.fragment_container, new Login());
            transaction.commit();
        }, SPLASH_DELAY);
    }

}