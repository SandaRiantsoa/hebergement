const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken'); 
const authController = require('./authController');
const roomController = require('./roomController');


const app = express();
const port = 3000;

// Définir le dossier public comme répertoire pour les fichiers statiques
app.use(express.static('public'));

// Middleware pour parser le corps des requêtes
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Middleware d'authentification pour vérifier le token
function authenticateToken(req, res, next) {
    const token = req.header('Authorization');
    //query
    if (!token) {
      return res.status(401).json({ error: 'Token manquant.' });
    }
  
    jwt.verify(token, 'hotel-tok', (err, user) => {
      if (err) {
        return res.status(403).json({ error: 'Token invalide.' });
      }
  
      req.user = user;
      next();
    });
  }
  

// Route pour l'inscription d'un utilisateur
app.post('/api/register', async (req, res) => {
  const { username, password, isAdmin } = req.body;

  try {
    const message = await authController.registerUser(username, password, isAdmin);
    return res.status(201).json({ message });
  } catch (error) {
    console.error('Erreur lors de l\'enregistrement de l\'utilisateur :', error);
    return res.status(400).json({ error: error.message });
  }
});

// Route pour la connexion d'un utilisateur
app.post('/api/login', async (req, res) => {
  const { username, password } = req.body;
  try {
    const message = await authController.loginUser(username, password);
    return res.status(200).json({ message });
  } catch (error) {
    console.error('Erreur lors de la connexion de l\'utilisateur :', error);
    return res.status(401).json({ error: error.message });
  }
});
// Route pour la création d'une nouvelle chambre (accessible aux utilisateurs avec le profil "admin")
app.post('/api/rooms', authenticateToken, roomController.createRoom);

// Route pour lister toutes les chambres (accessible à tout utilisateur connecté)
app.get('/api/rooms', authenticateToken, roomController.listRooms);

  
// Démarrer le serveur
app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});
