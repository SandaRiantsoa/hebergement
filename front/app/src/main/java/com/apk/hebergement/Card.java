package com.apk.hebergement;

import android.net.Uri;
import android.widget.MediaController;
import android.widget.VideoView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class Card extends AppCompatActivity {
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        videoView = findViewById(R.id.videoView);

        // Charger une vidéo à partir du répertoire "res/raw" de votre projet
        String videoPath = "android.resource://" + getPackageName() + "/" + R.raw.girl;
        videoView.setVideoURI(Uri.parse(videoPath));

        // Ajouter un MediaController pour permettre la lecture et la pause de la vidéo
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        // Commencer la lecture de la vidéo
        videoView.start();

        
    }
}