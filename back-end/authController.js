const bcrypt = require('bcrypt');
const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const jwt = require('jsonwebtoken'); // Importer la bibliothèque jsonwebtoken

// Créer une instance de la base de données JSON
const adapter = new FileSync('database.json');
const db = lowdb(adapter);

// Fonction de validation personnalisée pour vérifier si les champs sont non vides
function validateRequiredFields(fields) {
  return fields.every(field => field !== undefined && field !== null && field.trim() !== '');
}

// Fonction de validation personnalisée pour vérifier si le mot de passe répond aux critères
function validatePassword(password) {
  // Vérifier que le mot de passe a au moins 8 caractères
  if (password.length < 8) {
    return false;
  }

  // Vous pouvez ajouter d'autres critères ici (par exemple, au moins une majuscule, un chiffre, etc.)
  return true;
}
// Fonction pour enregistrer un nouvel utilisateur avec un profil spécifié (par défaut : "client")
async function registerUser(username, password, isAdmin = false) {
    // Vérifier si les champs sont non vides
    if (!validateRequiredFields([username, password])) {
      throw new Error('Les champs ne peuvent pas être vides.');
    }
  
    // Vérifier si l'utilisateur existe déjà
    const existingUser = db.get('users').find({ username }).value();
    if (existingUser) {
      throw new Error('Cet utilisateur existe déjà.');
    }
  
    // Vérifier si le mot de passe répond aux critères
    if (!validatePassword(password)) {
      throw new Error('Le mot de passe doit avoir au moins 8 caractères.');
    }
  
    try {
      // Crypter le mot de passe avant de le stocker
      const saltRounds = 10;
      const hashedPassword = await bcrypt.hash(password, saltRounds);
  
      // Stocker l'utilisateur dans la base de données avec le profil spécifié
      const profile = isAdmin ? 'admin' : 'client';
      db.get('users')
        .push({ username, password: hashedPassword, profile })
        .write();
  
      return 'Utilisateur enregistré avec succès.';
    } catch (error) {
      console.error('Erreur lors de l\'enregistrement de l\'utilisateur :', error);
      throw new Error('Erreur lors de l\'enregistrement de l\'utilisateur.');
    }
}

// Fonction pour connecter un utilisateur
async function loginUser(username, password) {
  try {
     // Vérifier si les champs sont non vides
    if (!validateRequiredFields([username, password])) {
        throw new Error('Les champs ne peuvent pas être vides.');
    }
    // Chercher l'utilisateur dans la base de données
    const user = db.get('users').find({ username }).value();
    if (!user) {
      throw new Error('Utilisateur non trouvé.');
    }

    // Vérifier si le mot de passe est correct
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      throw new Error('Mot de passe incorrect.');
    }

    // Créer le token JWT avec l'ID de l'utilisateur et le profil
    const tokenPayload = { id: user.id, profile: user.profile };
    const secretKey = 'hotel-tok';
    const token = jwt.sign(tokenPayload, secretKey); 
    return token;
  } catch (error) {
    console.error('Erreur lors de la connexion de l\'utilisateur :', error);
    throw new Error('Erreur lors de la connexion de l\'utilisateur.');
  }
}

module.exports = {
  registerUser,
  loginUser,
};
