package com.apk.hebergement;

import com.apk.hebergement.data.model.Room;
import com.apk.hebergement.data.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiService {
    @POST("login/")
    Call<ReponseServer> getUser(@Body User json);

    @POST("register/")
    Call<ReponseServer> register(@Body User json);

    @GET("rooms/")
    Call<Room[]> rooms(@Header("Authorization") String token);
}
