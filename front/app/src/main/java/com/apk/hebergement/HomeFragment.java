package com.apk.hebergement;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.apk.hebergement.data.model.Room;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class HomeFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public HomeFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static HomeFragment newInstance(int columnCount) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            SharedPreferences sharedPreferences = requireContext().getSharedPreferences(Service.sharedPreferences, Context.MODE_PRIVATE);
            String token = sharedPreferences.getString("token" , "");
            this.setRooms(token, recyclerView);
        }
        return view;
    }

    private void setRooms (String token, RecyclerView recyclerView) {
        Call<Room[]> call = Service.API_SERVICE.rooms(token);

        call.enqueue(new Callback<Room[]>() {

            @Override
            public void onResponse(Call<Room[]> call, Response<Room[]> response) {
                if (response.isSuccessful()) {
                    List<Room> rooms = Arrays.asList(response.body());
                    recyclerView.setAdapter(new MyHomeRecyclerViewAdapter(rooms, getParentFragmentManager().beginTransaction()));
                } else {
                    System.out.println(response.message());
                }
            }

            @Override
            public void onFailure(Call<Room[]> call, Throwable t) {
                System.out.println("FAILURE");
            }
        });
    }

}