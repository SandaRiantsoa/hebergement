package com.apk.hebergement;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookingCom#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingCom extends Fragment {
    private WebView booking;

    public BookingCom() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BookingCom.
     */
    // TODO: Rename and change types and number of parameters
    public static BookingCom newInstance() {
        BookingCom fragment = new BookingCom();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_com, container, false);
        booking = (WebView) view.findViewById(R.id.booking);
        booking.setWebViewClient(new WebViewClient());
        booking.loadUrl("https://www.booking.com/");
        return view;
    }
}