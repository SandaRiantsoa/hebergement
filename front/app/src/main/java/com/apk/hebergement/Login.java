package com.apk.hebergement;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.apk.hebergement.data.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Login#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class Login extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_username = "username";
    private static final String ARG_password = "password";

    // TODO: Rename and change types of parameters
    private String musername;
    private String mpassword;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param username Parameter 1.
     * @param password Parameter 2.
     * @return A new instance of fragment Login.
     */
    // TODO: Rename and change types and number of parameters
    public static Login newInstance(String username, String password) {
        Login fragment = new Login();
        Bundle args = new Bundle();
        args.putString(ARG_username, username);
        args.putString(ARG_password, password);
        fragment.setArguments(args);
        return fragment;
    }

    public Login() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            musername = getArguments().getString(ARG_username);
            mpassword = getArguments().getString(ARG_password);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        EditText usernameEditText = view.findViewById(R.id.username);
        musername = usernameEditText.getText().toString();

        EditText passwordEditText = view.findViewById(R.id.password);
        mpassword = passwordEditText.getText().toString();

        TextView error = view.findViewById(R.id.error);

        VideoView videoView = view.findViewById(R.id.videoView);

        String videoPath = "android.resource://" + getContext().getPackageName() + "/" + R.raw.video;
        videoView.setVideoURI(Uri.parse(videoPath));
        MediaController mediaController = new MediaController(getContext());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);

        Button btnSeConnecter = view.findViewById(R.id.btnSeConnecter);
        btnSeConnecter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User data = new User(usernameEditText.getText().toString(), passwordEditText.getText().toString());
                Call<ReponseServer> call = Service.API_SERVICE.getUser(data);

                call.enqueue(new Callback<ReponseServer>() {
                    @Override
                    public void onResponse(Call<ReponseServer> call, Response<ReponseServer> response) {
                        if (response.isSuccessful()) {
                            String token = response.body().getMessage();
                            try {
                                SharedPreferences sharedPreferences = requireContext().getSharedPreferences(Service.sharedPreferences, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("token", token);
                                editor.apply();
                                replaceFragment(new HomeFragment());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                                String errorMessage = jsonObject.getString("error");
                                error.setText(errorMessage);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            System.out.println(response.body());
                        }
                    }
                    @Override
                    public void onFailure(Call<ReponseServer> call, Throwable t) {
                        error.setText(t.getMessage());
                    }
                });



            }
        });

        Button register = view.findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("Inscription bouton");
                replaceFragment(new Register());
            }
        });

        return view;
    }

    private void replaceFragment(Fragment fragment) {
        if (getParentFragmentManager() != null) {
            FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null); // Ajouter la transaction à la pile de retour
            transaction.commit();
        }
    }
}