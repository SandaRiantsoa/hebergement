const lowdb = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

// Créer une instance de la base de données JSON pour les chambres
const adapter = new FileSync('rooms.json');
const db = lowdb(adapter);

// Ajouter une collection 'rooms' à la base de données s'il n'en existe pas encore
if (!db.has('rooms').value()) {
  db.defaults({ rooms: [] }).write();
}
// Fonction pour créer une nouvelle chambre (accessible uniquement aux utilisateurs avec le profil "admin")
function createRoom(req, res) {
    // Vérifier si l'utilisateur est authentifié en tant qu'administrateur
    const user = req.user;
    if (!user || user.profile !== 'admin') {
      return res.status(403).json({ error: 'Vous n\'avez pas les autorisations pour créer une chambre.' });
    }
  
    try {
      // Ajouter la chambre à la base de données
      const room = {
        name: req.body.name,
        description: req.body.description,
        image: req.body.image,
        availability: req.body.availability,
        price: req.body.price,
        roomNumber: req.body.roomNumber,
        capacity: req.body.capacity,
      };
      db.get('rooms').push(room).write();
  
      return res.status(201).json({ message: 'Chambre créée avec succès.' });
    } catch (error) {
      console.error('Erreur lors de la création de la chambre :', error);
      return res.status(500).json({ error: 'Erreur lors de la création de la chambre.' });
    }
  }
  
  // Fonction pour lister toutes les chambres (accessible à tout utilisateur connecté)
  function listRooms(req, res) {
    // Vérifier si l'utilisateur est authentifié
    const user = req.user;
    if (!user) {
      return res.status(401).json({ error: 'Vous devez être connecté pour accéder à la liste des chambres.' });
    }
  
    try {
        // Récupérer les paramètres de requête pour la recherche avancée
        const { name, availability, price, capacity } = req.query;

        // Filtrer les chambres en fonction des critères de recherche
        let filteredRooms = db.get('rooms').value();

        if (name) {
        // Filtre par nom de chambre
        filteredRooms = filteredRooms.filter(room => room.name.toLowerCase().includes(name.toLowerCase()));
        }

        if (availability) {
        // Filtre par disponibilité
        filteredRooms = filteredRooms.filter(room => room.availability === availability);
        }

        if (price) {
        // Filtre par prix (supérieur ou égal au prix donné)
        filteredRooms = filteredRooms.filter(room => room.price >= parseInt(price));
        }

        if (capacity) {
        // Filtre par capacité (supérieure ou égale à la capacité donnée)
        filteredRooms = filteredRooms.filter(room => room.capacity >= parseInt(capacity));
        }

        return res.status(200).json(filteredRooms);
    } catch (error) {
      console.error('Erreur lors de la récupération des chambres :', error);
      return res.status(500).json({ error: 'Erreur lors de la récupération des chambres.' });
    }
  }
  
  module.exports = {
    createRoom,
    listRooms,
  };